# README / Tutorials
*Last update, 18-03-2019*

This repository holds a set of tutorials to help the users of the ICI-CNSC to better understand or simply use our platform with the best practices.

This is the main page of the documentation for this repository, which relies on MkDocs and the Read the Docs theme. In particular, the latest version of these tutorials is available online:

http://cnscfr-tutorials.rtfd.io


## Repo

- https://gitlab.com/cnscfr/tutorials under *cnscfr* gitlab group
- https://readthedocs.org/projects/cnsc-tutorials 

## How to use this website
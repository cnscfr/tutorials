.. CNSC Tutorials documentation master file, created by
   sphinx-quickstart on Tue Mar 19 17:40:27 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CNSC HPC Tutorials's documentation!
==============================================

     Copyright (c) 2015-2019 CNSC HPC Team <ici-sc@ec-nantes.fr>

This repository holds a set of tutorials to help the users of the [CNSC](https://supercomputing.ec-nantes.fr) platform to better understand or simply use our platform.

This is the main page of the documentation for this repository, which relies on [MkDocs](http://www.mkdocs.org/) and the [Read the Docs](http://readthedocs.io) theme.
In particular, the latest version of these tutorials is available online:

 <http://cnsc-tutorials.rtfd.io>

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
